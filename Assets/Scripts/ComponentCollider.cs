﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ComponentCollider : MonoBehaviour
{
    public event Action onMouseDown;
    public event Action onRightMouseDown;

    private void OnMouseDown()
    {
        if (onMouseDown != null && !IsPointerOverUI())
        {
            onMouseDown();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && (onRightMouseDown != null) && !IsPointerOverUI())
        {
            onRightMouseDown();
        }
    }

    private bool IsPointerOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }
}
