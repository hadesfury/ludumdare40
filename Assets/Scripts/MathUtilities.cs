﻿using System;
using Random = UnityEngine.Random;

public class MathUtilities
{
    public static int GetRandomGaussian(int parMin, int parMax, int parDiceThrows = 10)
    {
        int locRandomTotal = 0;

        for (int locDiceThrowIndex = 0; locDiceThrowIndex < parDiceThrows; locDiceThrowIndex++)
        {
            locRandomTotal += Random.Range(parMin, parMax);
        }

        return locRandomTotal / parDiceThrows;
    }

    public static float GetRandomGaussian(float parMin, float parMax, int parDiceThrows = 5)
    {
        float locRandomTotal = 0;

        for (int locDiceThrowIndex = 0; locDiceThrowIndex < parDiceThrows; locDiceThrowIndex++)
        {
            locRandomTotal += Random.Range(parMin, parMax);
        }

        return locRandomTotal / parDiceThrows;
    }

    public static int GetCircumference(int parRadius)
    {
        return (int) (parRadius * 2 * Math.PI * parRadius);
    }
}
