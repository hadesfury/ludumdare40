﻿using UnityEngine;

public class GuiManager : MonoBehaviour
{
    [SerializeField] private WelcomeScreenUiLayout welcomeScreenUiLayout;
    [SerializeField] private IngameTopBarUiLayout ingameTopBarUiLayout;
    [SerializeField] private PlanetUiLayout planetUiLayout;
    [SerializeField] private IngameUiLayout ingameUiLayout;    
    [SerializeField] private GameOverScreenUiLayout gameOverScreenUiLayout;
    [SerializeField] private IntroScreenUiLayout introScreenUiLayout;


    private Animator planetUiAnimator;
    private Animator systemUiAnimator;

    private bool isGuiEnabled = true;
    
    private void Awake()
    {
        GameManager locGameManager = GameManager.gameManager;
        
        GameManager.gameManager.onSelectPlanet += OnSelectPlanet;
        GameManager.gameManager.onBackToSystem += OnBackToSystem;
        GameManager.gameManager.onSelectShip += OnSelectShip;
        
        planetUiAnimator = planetUiLayout.GetComponent<Animator>();
        systemUiAnimator = ingameUiLayout.GetComponent<Animator>();
        
        locGameManager.RegisterOnEnterEvent<WelcomeState>(OnWelcomeStateEnter);
        locGameManager.RegisterOnUpdateEvent<WelcomeState>(OnWelcomeStateUpdate);
        locGameManager.RegisterOnExitEvent<WelcomeState>( OnWelcomeStateExit );

        locGameManager.RegisterOnEnterEvent<IntroState>( OnIntroStateEnter );
        locGameManager.RegisterOnUpdateEvent<IntroState>( OnIntroStateUpdate );
        locGameManager.RegisterOnExitEvent<IntroState>( OnIntroStateExit );

        locGameManager.RegisterOnEnterEvent<IngameState>(OnGameStateEnter);
        locGameManager.RegisterOnUpdateEvent<IngameState>(OnGameStateUpdate);
        locGameManager.RegisterOnExitEvent<IngameState>( OnGameStateExit );

        locGameManager.RegisterOnEnterEvent<GameOverState>(OnGameOverStateEnter);
        locGameManager.RegisterOnUpdateEvent<GameOverState>( OnGameOverStateUpdate );
        locGameManager.RegisterOnExitEvent<GameOverState>( OnGameOverStateExit );

        OnWelcomeStateEnter();
    }

    


    private void OnWelcomeStateEnter()
    {
        welcomeScreenUiLayout.gameObject.SetActive(true);
        planetUiLayout.gameObject.SetActive(false);
        ingameUiLayout.gameObject.SetActive(false);
        gameOverScreenUiLayout.gameObject.SetActive(false);
        ingameTopBarUiLayout.gameObject.SetActive(false);
        introScreenUiLayout.gameObject.SetActive( false );
    }

    private void OnWelcomeStateUpdate(float parElapsedTime)
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            GameManager.gameManager.HandleMessage(MessageType.GAME_INTRO);
        }
    }

    private void OnWelcomeStateExit()
    {
        welcomeScreenUiLayout.gameObject.SetActive( false );
    }



    private void OnIntroStateEnter()
    {
        introScreenUiLayout.gameObject.SetActive( true );
    }

    private void OnIntroStateUpdate( float parElapsedTime )
    {
        
    }

    private void OnIntroStateExit()
    {
        introScreenUiLayout.gameObject.SetActive( false );
    }



    private void OnGameStateEnter()
    {
        isGuiEnabled = true;

        planetUiLayout.gameObject.SetActive(isGuiEnabled);
        ingameUiLayout.gameObject.SetActive(isGuiEnabled);
        ingameTopBarUiLayout.gameObject.SetActive(isGuiEnabled);
    }

    private void OnGameStateUpdate( float deltaTime )
    {
        if (Input.GetKeyDown(KeyCode.F8))
        {
            isGuiEnabled = !isGuiEnabled;
        }

        planetUiLayout.gameObject.SetActive(isGuiEnabled);
        ingameUiLayout.gameObject.SetActive(isGuiEnabled);
        ingameTopBarUiLayout.gameObject.SetActive(isGuiEnabled);
    }

    private void OnGameStateExit()
    {
        planetUiLayout.gameObject.SetActive( false );
        ingameUiLayout.gameObject.SetActive( false );
    }

    private void OnGameOverStateEnter()
    {
        gameOverScreenUiLayout.gameObject.SetActive(true);
        GameManager.gameManager.SelectPlanet( null );
    }

    private void OnGameOverStateExit()
    {
        gameOverScreenUiLayout.gameObject.SetActive( false );
    }

    private void OnGameOverStateUpdate(float parElapsedTime)
    {
        if( Input.GetKeyUp( KeyCode.Space ) )
        {
            GameManager.gameManager.HandleMessage( MessageType.RESET );
        }
    }

    private void OnSelectPlanet()
    {
//        Debug.Log( "GUI onSelectPlanet()" );

        PlanetLayout locPlanetLayout = GameManager.gameManager.GetSelectedPlanet();
        
        planetUiLayout.SetData(locPlanetLayout);
        planetUiAnimator.Play( "In" );
        systemUiAnimator.Play( "In" );
    }

    private void OnBackToSystem()
    {
        planetUiAnimator.Play( "Out" );
        systemUiAnimator.Play( "In" );
    }

    private void OnSelectShip()
    {
        planetUiAnimator.Play( "Out" );
        systemUiAnimator.Play( "Out" );
    }


}
