﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    private static readonly int GAME_TIME = 6 * 60;
    private static readonly int PLANET_NAME_MIN_SIZE = 1;
    private static readonly int PLANET_NAME_MAX_SIZE = 4;
    private static readonly int STARTING_POPULATION = 20;
    private static readonly int STARTING_RESOURCES = 800;

    [SerializeField] private SolarSystemLayout solarSystemLayout;
    [SerializeField] private TextAsset nameGeneratorDataFile;

    public event Action onSelectPlanet;
    public event Action onBackToSystem;
    public event Action onSelectShip;

    private PlanetLayout selectedPlanet;

    public static GameManager gameManager;

    private readonly Dictionary<Type,State> stateTable = new Dictionary<Type, State>();
    private State currentState;

    private float gameTimer;

    private PlanetLayout motherlandPlanetLayout;
    
    private readonly List<string> usedPlanetNameTable = new List<string>();
    private readonly List<string> planetNameSylableTable = new List<string>();
    
    private readonly Queue<string> notificationTable = new Queue<string>();

    private CameraFocus currentCamera;

    private void Awake()
    {
        gameManager = this;
        
        RegisterState<WelcomeState>();
        RegisterState<IntroState>();
        RegisterState<IngameState>();
        RegisterState<GameOverState>();

        currentState = GetState<WelcomeState>();
        
        RegisterOnEnterEvent<IngameState>(OnGameStateEnter);
        RegisterOnUpdateEvent<IngameState>(OnGameStateUpdate);
        
        char[] locEndOfLineSeparator = {'\r','\n',';'};
        string[] locNameLineTable = nameGeneratorDataFile.text.Split(locEndOfLineSeparator, StringSplitOptions.RemoveEmptyEntries);

        foreach (string locName in locNameLineTable)
        {
                planetNameSylableTable.Add(locName);
        }

        GoToSystemCamera();
    }

    private void FixedUpdate()
    {
        currentState.OnUpdateState( Time.fixedDeltaTime, this );
    }

    private void GoToPlanetCamera( PlanetLayout planet )
    {
        SwitchToCamera( planet.GetPlanetCamera() );

        if( onSelectPlanet != null )
        {
            onSelectPlanet();
        }
    }

    private void GoToSystemCamera()
    {
        SwitchToCamera(solarSystemLayout.GetSystemCamera());

        if( onBackToSystem != null )
        {
            onBackToSystem();
        }
    }

    public string GetUniquePlanetName()
    {
        StringBuilder locPlanetNameStringBuilder = new StringBuilder();

        do
        {
            int locPlanetNameSize = Random.Range(PLANET_NAME_MIN_SIZE, PLANET_NAME_MAX_SIZE);

            for (int locSyllableIndex = 0; locSyllableIndex < locPlanetNameSize; locSyllableIndex++)
            {
                string locCurrentSyllable = planetNameSylableTable[Random.Range(0, planetNameSylableTable.Count)];

                locPlanetNameStringBuilder.Append(locCurrentSyllable);
            }
        }
        while (!IsPlanetNameUnique(locPlanetNameStringBuilder.ToString()));

        usedPlanetNameTable.Add(locPlanetNameStringBuilder.ToString());
        
        return locPlanetNameStringBuilder.ToString();
    }

    private bool IsPlanetNameUnique(string parPlanetName)
    {
        bool locIsPlanetNameUnique = true;
        
        foreach (string locPlanetName in usedPlanetNameTable)
        {
            if (string.Equals(parPlanetName, locPlanetName, StringComparison.CurrentCultureIgnoreCase))
            {
                locIsPlanetNameUnique = false;
                
                break;
            }
        }

        return locIsPlanetNameUnique;
    }

    private void SwitchToCamera( CameraFocus parCamera )
    {
        if( currentCamera != null )
        {
            currentCamera.SetActiveCamera( false );
        }

        currentCamera = parCamera;
        currentCamera.SetActiveCamera( true );
    }

    private void OnGameStateEnter()
    {
        gameTimer = 0.0f;
        
        {
            int motherland = ( int ) Mathf.Floor( UnityEngine.Random.Range( 0, GameManager.gameManager.GetSolarSystemLayout().GetPlanetLayouts().Count ) );
            motherlandPlanetLayout = GameManager.gameManager.GetSolarSystemLayout().GetPlanetLayouts()[ motherland ];

            Debug.Log( "Choosing motherland : " + motherland );

            motherlandPlanetLayout.Planet.SetName( "Motherland" );

            if( motherlandPlanetLayout.Planet.GetPopulationGrowthFactor() < 1 )
            {
                motherlandPlanetLayout.Planet.SetPopulationGrowthFactor( -motherlandPlanetLayout.Planet.GetPopulationGrowthFactor() );
            }

            motherlandPlanetLayout.Planet.ReceivePopulationShipment(STARTING_POPULATION);
            motherlandPlanetLayout.Planet.ReceiveResourceShipment(STARTING_RESOURCES);
        }
    }
    
    private void OnGameStateUpdate(float parElapsedTime)
    {
        gameTimer += parElapsedTime;

        if (gameTimer >= GAME_TIME)
        {
            ChangeState<GameOverState>();
        }
    }

    private void RegisterState<TYPE_STATE>() where TYPE_STATE : State
    {
        State locState = new GameObject(typeof(TYPE_STATE).Name).AddComponent<TYPE_STATE>();
        
        locState.transform.SetParent(transform, false);
        stateTable.Add(typeof(TYPE_STATE), locState);
    }

    private State GetState<TYPE_STATE>() where TYPE_STATE : State
    {
        return stateTable[typeof(TYPE_STATE)];
    }

    public void ChangeState<TYPE_STATE>() where TYPE_STATE : State
    {
        if (currentState != null)
        {
            currentState.OnExitState(this);
        }

        currentState = GetState<TYPE_STATE>();
        currentState.OnEnterState(this);
    }

    public bool IsCurrentState<TYPE_STATE>() where TYPE_STATE : State
    {
        return currentState == GetState<TYPE_STATE>();
    }

    public virtual void SelectPlanet( PlanetLayout parPlanet )
    {
        if( IsCurrentState<IngameState>() )
        {
            selectedPlanet = parPlanet;

            if( parPlanet != null )
            {
                GoToPlanetCamera( selectedPlanet );
            }

            else
            {
                GoToSystemCamera();
            }
        }

        else if( parPlanet == null )
        {
            selectedPlanet = parPlanet;
            GoToSystemCamera();
        }
    }

    public void SelectShip(Ship parShip)
    {
        if( parShip != null )
        {
            SwitchToCamera( parShip.GetCamera() );

            if( onSelectShip != null )
            {
                onSelectShip();
            }
        }
    }

    public PlanetLayout GetPlanetLayout( Planet parPlanet )
    {
        foreach( PlanetLayout locPlanetLayout in solarSystemLayout.GetPlanetLayouts() )
        {
            if( locPlanetLayout.Planet == parPlanet )
            {
                return locPlanetLayout;
            }
        }

        return null;
    }

    public PlanetLayout GetSelectedPlanet()
    {
        return selectedPlanet;
    }

    public int GetTotalCollectedResources ()
    {
        int locTotal = 0;
        foreach( PlanetLayout locPlanet in solarSystemLayout.GetPlanetLayouts() )
        {
            locTotal += locPlanet.Planet.GetCollectedResources();
        }

        return locTotal;
    }

    public int GetTotalPopulation()
    {
        int locTotal = 0;
        foreach( PlanetLayout locPlanet in solarSystemLayout.GetPlanetLayouts() )
        {
            locTotal += locPlanet.Planet.GetPopulation();
        }

        return locTotal;
    }

    public int GetRemainingTimeInSeconds()
    {
        return Mathf.RoundToInt( GAME_TIME - gameTimer );
    }

    public SolarSystemLayout GetSolarSystemLayout()
    {
        return solarSystemLayout;
    }

    public PlanetLayout GetMotherlandPlanetLayout()
    {
        return motherlandPlanetLayout;
    }

    public void RegisterOnEnterEvent<TYPE_STATE>(Action parAction) where TYPE_STATE : State
    {
        State locState = GetState<TYPE_STATE>();

        locState.onEnterEvent += parAction;
    }

    public void RegisterOnExitEvent<TYPE_STATE>(Action parAction) where TYPE_STATE : State
    {
        State locState = GetState<TYPE_STATE>();

        locState.onExitEvent += parAction;
    }

    public void RegisterOnUpdateEvent<TYPE_STATE>(Action<float> parAction) where TYPE_STATE : State
    {
        State locState = GetState<TYPE_STATE>();

        locState.onUpdateEvent += parAction;
    }

    public void HandleMessage(MessageType parMessageType)
    {
        currentState.HandleMessage(parMessageType, this);
    }

    public void Reset()
    {
        SceneManager.LoadScene(0);
    }

    public void PostNotification(string parNotificationText)
    {
        notificationTable.Enqueue(parNotificationText);
    }

    public Queue<string> GetNotificationTable()
    {
        return notificationTable;
    }

    public Camera GetActiveCamera()
    {
        Camera locActivueCamera = null;

        if (currentCamera != null)
        {
            locActivueCamera = currentCamera.GetComponent<Camera>();
        }

        return locActivueCamera;
    }
}