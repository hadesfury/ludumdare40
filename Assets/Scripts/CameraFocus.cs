﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraFocus : MonoBehaviour {

    [SerializeField] private float rotationSpeed = 2.0f;
    [SerializeField] private float zoomSpeed = 256.0f;
    [SerializeField] private float minDistance = 40.0f;
    [SerializeField] private AudioListener audioListener;
    [SerializeField] private float currentDistance = 80.0f;


    private Transform cameraTarget;
    

    private bool isActiveCamera = false;

    private void Awake()
    {
        audioListener = GetComponent<AudioListener>();

        cameraTarget = gameObject.transform.parent;
        gameObject.transform.LookAt( cameraTarget );
    }

    private void Update()
    {
        if (isActiveCamera && !EventSystem.current.IsPointerOverGameObject())
        {
            currentDistance -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

            if (currentDistance < minDistance)
            {
                currentDistance = minDistance;
            }

            UpdateCameraPosition();

            if (Input.GetMouseButton(1))
            {
                float locHorizontal = rotationSpeed * Input.GetAxis("Mouse X");
                float locVertical = rotationSpeed * Input.GetAxis("Mouse Y");

                transform.RotateAround(cameraTarget.position, transform.up, locHorizontal);
                transform.RotateAround(cameraTarget.position, transform.right, locVertical);

                gameObject.transform.LookAt(cameraTarget);
            }
        }
    }

    public void SetActiveCamera( bool value )
    {
        isActiveCamera = value;
        gameObject.SetActive( value );
        audioListener.enabled = value;

        UpdateCameraPosition();
    }

    public bool IsActiveCamera()
    {
        return isActiveCamera;
    }

    private void UpdateCameraPosition()
    {
        transform.position = cameraTarget.position + ( transform.position - cameraTarget.position ).normalized * currentDistance;
    }
}
