﻿
public class WelcomeState : State
{
    public override bool HandleMessage(MessageType parMessageType, GameManager parGameManager)
    {
        if (parMessageType == MessageType.GAME_INTRO)
        {
            parGameManager.ChangeState<IntroState>();
            
            return true;
        }
        
        return base.HandleMessage(parMessageType, parGameManager);
    }
}