﻿public class GameOverState : State
{
    public override bool HandleMessage(MessageType parMessageType, GameManager parGameManager)
    {
        if (parMessageType == MessageType.RESET)
        {
            parGameManager.Reset();
            return true;
        }
        
        return base.HandleMessage(parMessageType, parGameManager);
    }
}