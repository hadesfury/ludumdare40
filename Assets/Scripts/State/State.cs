﻿using System;
using UnityEngine;

public class State : MonoBehaviour
{
    public event Action onEnterEvent;
    public event Action<float> onUpdateEvent;
    public event Action onExitEvent;

    public void OnEnterState(GameManager parGameManager)
    {
        if (onEnterEvent != null)
        {
            onEnterEvent();
        }
    }
    
    public void OnUpdateState(float parDeltaTime, GameManager parGameManager)
    {
        if (onUpdateEvent != null)
        {
            onUpdateEvent(parDeltaTime);
        }
    }
    
    public void OnExitState(GameManager parGameManager)
    {
        if (onExitEvent != null)
        {
            onExitEvent();
        }
    }

    public virtual bool HandleMessage(MessageType parMessageType, GameManager parGameManager)
    {
        return false;
    }
}