﻿public class IntroState : State
{
    public override bool HandleMessage( MessageType parMessageType, GameManager parGameManager )
    {
        if( parMessageType == MessageType.GAME_START )
        {
            parGameManager.ChangeState<IngameState>();

            return true;
        }

        return base.HandleMessage( parMessageType, parGameManager );
    }
}
