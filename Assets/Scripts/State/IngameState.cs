﻿
public class IngameState : State
{
    public override bool HandleMessage(MessageType parMessageType, GameManager parGameManager)
    {
        if (parMessageType == MessageType.GAME_OVER)
        {
            parGameManager.ChangeState<GameOverState>();
            
            return true;
        }
        
        return base.HandleMessage(parMessageType, parGameManager);
    }
}