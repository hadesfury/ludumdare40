﻿public class IncrementalId
{
    private long nextId;

    public IncrementalId(long parInitialValue = 1)
    {
        nextId = parInitialValue;
    }

    public long GetNextId()
    {
        return nextId++;
    }

    public void CheckMaxId(long parId)
    {
        if (parId >= nextId)
        {
            nextId = parId + 1;
        }
    }
}
