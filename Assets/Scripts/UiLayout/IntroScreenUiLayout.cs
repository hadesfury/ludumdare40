﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using TMPro;

public class IntroScreenUiLayout : MonoBehaviour
{

    public static readonly int INTRO_TEXT_DURATION = 10;
    public static readonly float TEXT_ANIMATION_DELAY = 0.5f;

    private int currentText = 0;

    [SerializeField] private List<TMP_Text> textList;
    [SerializeField] private TMP_Text skipText;
    [SerializeField] private GameObject blackOverlay;

	// Use this for initialization
	private void Start () {
		StartIntro();
	}
	
	// Update is called once per frame
    private void Update()
    {
        if( Input.GetKeyDown( KeyCode.Space ) )
        {
            ShowNextText();
        }

        if( Input.GetKeyDown( KeyCode.Escape ) )
        {
            FinishIntro();
        }
    }

    private void StartIntro()
    {
        blackOverlay.GetComponent<Animator>().Play( "In" );
        skipText.GetComponent<Animator>().Play( "In" );
        if( textList.Count == 0 )
        {
            FinishIntro();
        }

        else
        {
            textList[0].GetComponent<Animator>().Play( "In" );
        }
    }

    private void ShowNextText()
    {
        if( currentText < textList.Count - 1 )
        {
            textList[currentText].GetComponent<Animator>().Play( "Out" );
            currentText++;
            StartCoroutine( ShowCurrentTextCoroutine() );
        }

        else
        {
            FinishIntro();
        }
    }

    private IEnumerator ShowCurrentTextCoroutine()
    {
        yield return new WaitForSeconds( TEXT_ANIMATION_DELAY );

        textList[ currentText ].GetComponent<Animator>().Play( "In" );
    }

    private void FinishIntro()
    {
        textList[currentText].GetComponent<Animator>().Play( "Out" );
        blackOverlay.GetComponent<Animator>().Play( "Out" );
        skipText.GetComponent<Animator>().Play( "Out" );
        //nextText.GetComponent<Animator>().Play( "Out" );

        StartCoroutine( StartGameCoroutine() );
    }

    private IEnumerator StartGameCoroutine()
    {
        yield return new WaitForSeconds( TEXT_ANIMATION_DELAY );

        GameManager.gameManager.HandleMessage( MessageType.GAME_START );
    }
}
