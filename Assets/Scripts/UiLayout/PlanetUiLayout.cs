﻿using System.Collections.Generic;
using TMPro;
using UiLayout;
using UnityEngine;
using UnityEngine.UI;

public class PlanetUiLayout : MonoBehaviour
{
    [SerializeField] private Button backButton;
    [SerializeField] private TMP_Text planetName;
    [SerializeField] private TMP_Text unemployedCitizensText;
    [SerializeField] private TMP_Text planetStatusText;
    [SerializeField] private TMP_Text exploitableResourcesText;
    [SerializeField] private VerticalLayoutGroup citizenInfoPanel;
    [SerializeField] private VerticalLayoutGroup resourcesInfoPanel;
    [SerializeField] private PlanetDataText populationGrowthText;
    [SerializeField] private PlanetDataText resourceGrowthText;

    [Header("Mines")]
    [SerializeField] private VerticalLayoutGroup minesInfoPanel;
    [SerializeField] private TMP_Text mineCountText;
    [SerializeField] private TMP_Text workingMineCountText;
    [SerializeField] private TMP_Text yieldText;
    [SerializeField] private Button buildMineButton;
    [SerializeField] private TMP_Text mineButtonText;

    [Header("Ships")]
    [SerializeField] private VerticalLayoutGroup sendCitizenPanel;
    [SerializeField] private VerticalLayoutGroup sendResourcesPanel;
    [SerializeField] private TMP_Dropdown colonyShipDestinationDropdown;
    [SerializeField] private TMP_Dropdown cargoDestinationDropdown;
    [SerializeField] private Button sendCargoShipButton;
    [SerializeField] private TMP_Text sendCargoShipButtonText;
    [SerializeField] private Button sendColonyShipButton;
    [SerializeField] private TMP_Text sendColonyShipButtonText;

    private PlanetLayout selectedPlanet;

    private void Awake()
    {
        buildMineButton.onClick.AddListener(OnBuildMineButton);
        backButton.onClick.AddListener(OnBackButton);
        sendColonyShipButton.onClick.AddListener(OnSendColonyShipButton);
        sendCargoShipButton.onClick.AddListener(OnSendCargoShipButton);
    }

    private void OnEnable()
    {
        SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();

        foreach (PlanetLayout locPlanetLayout in locSolarSystemLayout.GetPlanetLayouts())
        {
            locPlanetLayout.Planet.onPlanetColonized += UpdateCargoDestinations;
        }
    }

    private void OnDisable()
    {
        SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();

        foreach (PlanetLayout locPlanetLayout in locSolarSystemLayout.GetPlanetLayouts())
        {
            locPlanetLayout.Planet.onPlanetColonized -= UpdateCargoDestinations;
        }
    }

    private void FixedUpdate()
    {
        if (selectedPlanet != null)
        {
            if( selectedPlanet.Planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED )
            {
                planetStatusText.SetText( "Colonized" );
                minesInfoPanel.gameObject.SetActive( true );
                citizenInfoPanel.gameObject.SetActive( true );
                resourcesInfoPanel.gameObject.SetActive( true );
                sendCitizenPanel.gameObject.SetActive( true );
                sendResourcesPanel.gameObject.SetActive( true );

                exploitableResourcesText.SetText( selectedPlanet.Planet.GetAvailableResources().ToString("D") );

                switch( selectedPlanet.Planet.GetPlanetAvailabeResourcesStatus() )
                {
                    case Planet.AvailabeResourcesStatus.ALRIGHT:
                        exploitableResourcesText.color = Color.white;
                        break;
                    case Planet.AvailabeResourcesStatus.WARNING:
                        exploitableResourcesText.color = Color.yellow;
                        break;
                    case Planet.AvailabeResourcesStatus.DANGER:
                        exploitableResourcesText.color = new Color(1.0f, 0.5f, 0f);
                        break;
                    case Planet.AvailabeResourcesStatus.EMPTY:
                        exploitableResourcesText.color = new Color( 1.0f, 0f, 0f );
                        break;
                }
        


                buildMineButton.interactable = selectedPlanet.Planet.GetCollectedResources() >= Planet.MINE_COST;
                mineButtonText.SetText(string.Format("Build (Cost : {0})",Planet.MINE_COST));
                mineCountText.SetText( selectedPlanet.Planet.GetMineCount().ToString( "D" ) );


                workingMineCountText.SetText( selectedPlanet.Planet.GetWorkingMineCount().ToString("D") 
                                              + " (" 
                                              + selectedPlanet.Planet.GetWorkingCitizens()
                                              + "/" 
                                              + selectedPlanet.Planet.GetTotalMineSlots()
                                              + ")" );



                yieldText.SetText( selectedPlanet.Planet.GetYield().ToString( "0" ) + "/s");

                unemployedCitizensText.SetText( selectedPlanet.Planet.GetUnemployedCitizens().ToString( "D" ) );

                populationGrowthText.UpdatePopulationDataFromPlanet(selectedPlanet.Planet);
                resourceGrowthText.UpdateResourceDataFromPlanet(selectedPlanet.Planet);

                sendColonyShipButton.interactable = selectedPlanet.Planet.GetPopulation() >= Planet.COLONY_SHIP_POPULATION_COUNT;
                                                    //&& (selectedPlanet.Planet.CollectedResources >= Planet.CARGO_SHIP_RESOURCE_COUNT);
                sendColonyShipButtonText.SetText(string.Format("Send colony ship ({0} pop)", Planet.COLONY_SHIP_POPULATION_COUNT));
                sendCargoShipButton.interactable = (selectedPlanet.Planet.GetCollectedResources() >= Planet.CARGO_SHIP_RESOURCE_COUNT)
                                                   && (GameManager.gameManager.GetSolarSystemLayout().GetColonizedPlanetTable().Count > 1);
                sendCargoShipButtonText.SetText(string.Format("Send cargo ship ({0} res)", Planet.CARGO_SHIP_RESOURCE_COUNT));
            }

            else
            {
                planetStatusText.SetText( "Not colonized" );
                exploitableResourcesText.SetText( selectedPlanet.Planet.GetAvailableResources().ToString( "D" ) );
                minesInfoPanel.gameObject.SetActive( false );
                citizenInfoPanel.gameObject.SetActive(false);
                resourcesInfoPanel.gameObject.SetActive( false );
                sendCitizenPanel.gameObject.SetActive( false );
                sendResourcesPanel.gameObject.SetActive( false );
            }

            exploitableResourcesText.SetText( selectedPlanet.Planet.GetAvailableResources().ToString("D") );
            
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButton();
        }
    }

    private void OnBuildMineButton()
    {
        if( selectedPlanet != null )
        {
            selectedPlanet.Planet.BuildMine();
        }
    }

    private void OnSendColonyShipButton()
    {
        if( selectedPlanet != null )
        {
            SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();
            PlanetLayout locDestinationPlanet = locSolarSystemLayout.GetPlanetByName(colonyShipDestinationDropdown.captionText.text);
            
            selectedPlanet.SendPopulationShipTo(locDestinationPlanet);
            
            //OnSendCargoShipButton();
        }
    }

    private void UpdateCargoDestinations(Planet parPlanet)
    {
        SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();
        List<PlanetLayout> locColonizedPlanetTable = locSolarSystemLayout.GetColonizedPlanetTable();
        List<string> locColonizedPlanetNameTable = new List<string>();
        
        cargoDestinationDropdown.ClearOptions();

        foreach (PlanetLayout locPlanetLayout in locColonizedPlanetTable)
        {
            if(selectedPlanet != locPlanetLayout)
            {
                locColonizedPlanetNameTable.Add(locPlanetLayout.Planet.GetName());
            }
        }
        cargoDestinationDropdown.interactable = locColonizedPlanetNameTable.Count > 0;
        cargoDestinationDropdown.AddOptions(locColonizedPlanetNameTable);
    }
    
    private void OnSendCargoShipButton()
    {
        if( selectedPlanet != null )
        {
            SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();
            PlanetLayout locDestinationPlanet = locSolarSystemLayout.GetPlanetByName(cargoDestinationDropdown.captionText.text);
            
            selectedPlanet.SendCargoShipTo(locDestinationPlanet);
        }
    }

    private void OnBackButton()
    {
        GameManager.gameManager.SelectPlanet( null );
    }

    public void SetData(PlanetLayout parPlanetLayout)
    {
        planetName.text = parPlanetLayout.Planet.GetName();
        selectedPlanet = parPlanetLayout;
        UpdateCargoDestinations(null);
        FillColonyShipDestinations();
    }

    private void FillColonyShipDestinations()
    {
        SolarSystemLayout locSolarSystemLayout = GameManager.gameManager.GetSolarSystemLayout();
        List<string> locColonizedPlanetNameTable = new List<string>();
        
        colonyShipDestinationDropdown.ClearOptions();

        foreach (PlanetLayout locPlanetLayout in locSolarSystemLayout.GetPlanetLayouts())
        {
            if(selectedPlanet != locPlanetLayout)
            {
                locColonizedPlanetNameTable.Add(locPlanetLayout.Planet.GetName());
            }
        }
        
        colonyShipDestinationDropdown.AddOptions(locColonizedPlanetNameTable);
    }
}