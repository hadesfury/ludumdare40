﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverScreenUiLayout : MonoBehaviour
{
    [SerializeField] private TMP_Text finalScoreText;
    [SerializeField] private TMP_Text bestScoreText;

    private void OnEnable()
    {
        int locScore = GameManager.gameManager.GetTotalCollectedResources();

        finalScoreText.SetText( "Final score : " + locScore.ToString("D") );

        int locBest = PlayerPrefs.GetInt( "BestScore", 0);

        if( locBest < locScore )
        {
            locBest = locScore;
            PlayerPrefs.SetInt( "BestScore", locBest );
        }

        bestScoreText.SetText( "Best score : " + locBest.ToString( "D" ) );
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
