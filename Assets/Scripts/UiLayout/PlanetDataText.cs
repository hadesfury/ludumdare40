﻿using System.Text;
using TMPro;
using UnityEngine;

namespace UiLayout
{
    public class PlanetDataText : MonoBehaviour
    {
        [SerializeField] private TMP_Text populationGrowthFactorText;

        public void UpdatePopulationDataFromPlanet(Planet parPlanet)
        {
            StringBuilder locPopulationStringBuilder = new StringBuilder();

            locPopulationStringBuilder.Append(parPlanet.GetPopulation());
            locPopulationStringBuilder.Append(" ");
            
            if (parPlanet.GetIsStarving())
            {
                locPopulationStringBuilder.Append("<color=\"red\"> ");
            }
            else
            {
                locPopulationStringBuilder.Append("<color=\"green\"> +");
            }
            
            locPopulationStringBuilder.Append(parPlanet.GetPopulationGrowthFactor());
            locPopulationStringBuilder.Append("/s");
            
            populationGrowthFactorText.SetText(locPopulationStringBuilder.ToString());
        }

        public void UpdateResourceDataFromPlanet(Planet parPlanet)
        {
            StringBuilder locPopulationStringBuilder = new StringBuilder();

            locPopulationStringBuilder.Append(parPlanet.GetCollectedResources());
            locPopulationStringBuilder.Append(" ");
            
            
            if( parPlanet.GetResourcesRatio() < 0 )
            {
                locPopulationStringBuilder.Append(" <color=red>" + parPlanet.GetResourcesRatio().ToString("F1") + "/s" );
            }

            else
            {
                locPopulationStringBuilder.Append(" <color=green>" + parPlanet.GetResourcesRatio().ToString("F1") + "/s" );
            }
            
            populationGrowthFactorText.SetText(locPopulationStringBuilder.ToString());
        }
    }
}