﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class IngameUiLayout : MonoBehaviour
{
    [SerializeField] private PlanetSummaryUiLayout planetSummaryUiPrefab;
    [SerializeField] private ScrollRect planetScrollRect;

    private readonly Dictionary<long, PlanetSummaryUiLayout> planetLayouts = new Dictionary<long, PlanetSummaryUiLayout>();

    private void Awake()
    {
        foreach (PlanetLayout locPlanetLayout in GameManager.gameManager.GetSolarSystemLayout().GetPlanetLayouts())
        {
            Planet locPlanet = locPlanetLayout.Planet;

            AddPlanetSummaryUiLayout(locPlanet, planetScrollRect.content);

            locPlanet.onPlanetStatusChanged += OnPlanetStatusChanged;
        }
    }

    private void Update()
    {
        if( GameManager.gameManager.GetTotalPopulation() <= 0 )
        {
            GameManager.gameManager.ChangeState<GameOverState>();
        }
    }

    private void OnPlanetStatusChanged( Planet.PlanetStatus parOldStatus, Planet.PlanetStatus parNewStatus, Planet parPlanet )
    {
        Debug.Log(string.Format("Planet {0} StatusChanged : {1} -> {2}", parPlanet.GetName(), parOldStatus, parNewStatus));
    }

    private void AddPlanetSummaryUiLayout(Planet parPlanet, Transform parDock)
    {
        PlanetSummaryUiLayout locPlanetSummaryUiLayout = GameObject.Instantiate(planetSummaryUiPrefab, parDock);
        
        locPlanetSummaryUiLayout.AssignPlanet(parPlanet);
        planetLayouts.Add(parPlanet.GetId(), locPlanetSummaryUiLayout);
    }
}
