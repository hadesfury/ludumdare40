﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IngameTopBarUiLayout : MonoBehaviour
{
    private readonly static float NOTIFICATION_DISPLAY_DURATION = 3.0f; 
    [SerializeField] private TMP_Text timeLeftText;
    [SerializeField] private TMP_Text notificationText;
    [SerializeField] private TMP_Text populationCountText;
    [SerializeField] private TMP_Text resoureCountText;
    [SerializeField] private int timesRunningOutThreshold = 60;
    [SerializeField] private Color timesRunningOutTextColor = Color.red;

    private Coroutine currentCoroutine;
    
    private void Awake()
    {
        notificationText.gameObject.SetActive(false);
    }

    private void Update()
    {
        int locRemainingTimeInSeconds = GameManager.gameManager.GetRemainingTimeInSeconds();

        timeLeftText.SetText( string.Format( "{0}:{1:D2}", Mathf.FloorToInt( locRemainingTimeInSeconds / 60 ), locRemainingTimeInSeconds % 60 ) );

        populationCountText.SetText( GameManager.gameManager.GetTotalPopulation().ToString( "D" ) );
        resoureCountText.SetText( GameManager.gameManager.GetTotalCollectedResources().ToString( "D" ) );

        if( locRemainingTimeInSeconds <= 60 )
        {
            timeLeftText.color = timesRunningOutTextColor;
        }

        if( currentCoroutine == null )
        {
            Queue<string> locNotificationTable = GameManager.gameManager.GetNotificationTable();
            if( locNotificationTable.Count > 0 )
            {
                ShowNotification( locNotificationTable.Dequeue() );
            }
        }
    }

    private void ShowNotification(string parNotificationText)
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
        }
        
        notificationText.gameObject.SetActive(true);
        notificationText.SetText(parNotificationText);
        currentCoroutine = StartCoroutine(NotificationHideTimer());
    }

    private IEnumerator NotificationHideTimer()
    {
        yield return new WaitForSeconds(NOTIFICATION_DISPLAY_DURATION);
        
        notificationText.gameObject.SetActive(false);

        currentCoroutine = null;
    }
}