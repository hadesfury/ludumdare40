﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UiLayout;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlanetSummaryUiLayout : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TextMeshProUGUI planetNameLabel;
    [SerializeField] private TMP_Text colonisedStatusText;
    [SerializeField] private TextMeshProUGUI planetAvailableResourcesLabel;
    [SerializeField] private TextMeshProUGUI planetPopulationLabel;
    [SerializeField] private RectTransform populationPanel;
    [SerializeField] private PlanetDataText populationGrowthText;
    [SerializeField] private PlanetDataText resourceGrowthText;
    [SerializeField] private Button focusButton;

    private Planet planet;

    private void Awake()
    {
        focusButton.onClick.AddListener(OnFocusButton);
    }

    private void Update()
    {
        if (planet != null)
        {
            planetNameLabel.SetText(planet.GetName());
            planetAvailableResourcesLabel.SetText(planet.GetAvailableResources().ToString("D"));
            planetPopulationLabel.SetText(planet.GetPopulation().ToString("D"));

            populationPanel.gameObject.SetActive(planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED);

            if (planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED)
            {
                colonisedStatusText.SetText("<color=\"green\">Colonized</color>");
            }
            else
            {
                colonisedStatusText.SetText("<color=\"red\">Not Colonized</color>");
            }

            populationGrowthText.UpdatePopulationDataFromPlanet(planet);
            resourceGrowthText.UpdateResourceDataFromPlanet(planet);
        }
    }

    public void AssignPlanet(Planet parPlanet)
    {
        planet = parPlanet;
    }

    private void OnFocusButton()
    {
        GameManager.gameManager.SelectPlanet( GameManager.gameManager.GetPlanetLayout( planet ) );
    }

    public Planet Planet
    {
        get { return planet; }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (planet != null)
        {
            planet.layout.Highlight(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (planet != null)
        {
            planet.layout.Highlight(false);
        }
    }
}
