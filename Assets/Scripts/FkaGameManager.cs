﻿using System;
using UnityEngine;

public class FkaGameManager : GameManager
{
    private PlanetLayout sourcePlanet;
    private PlanetLayout targetPlanet;

    [SerializeField]
    private Trajectory trajectoryPrefab;

    private Trajectory currentTrajectory;

    private static Color normalColor = new Color(0.5f, 0.5f, 0.5f, 1);
    private static Color sourceColor = new Color(0.0f, 1.0f, 0.0f, 1);
    private static Color targetColor = new Color(1.0f, 0.0f, 0.0f, 1);
    private static Color trajectoryColor = new Color(0.0f, 0.0f, 1.0f, 1);


    private void Awake()
    {
        gameManager = this;
    }

    private void FixedUpdate()
    {
        
    }


    public override void SelectPlanet(PlanetLayout parPlanet)
    {

        if (sourcePlanet == null) // select source
        {
            sourcePlanet = parPlanet;
            sourcePlanet.GetComponentInChildren<LineRenderer>().SetColors(sourceColor, sourceColor);
        }
        else if (parPlanet == targetPlanet)
        {
            targetPlanet.GetComponentInChildren<LineRenderer>().SetColors(normalColor, normalColor);
            GameObject.Destroy(currentTrajectory.gameObject);
            currentTrajectory = null;
            targetPlanet = null;

        }
        else if (parPlanet == sourcePlanet) // unselect source
        {
            sourcePlanet.GetComponentInChildren<LineRenderer>().SetColors(normalColor, normalColor);
            if (targetPlanet != null)
            {
                targetPlanet.GetComponentInChildren<LineRenderer>().SetColors(normalColor, normalColor);
                GameObject.Destroy(currentTrajectory.gameObject);
                currentTrajectory = null;
                targetPlanet = null;
            }
            sourcePlanet = null;
        }
        else
        {
            if (targetPlanet != null)
            {
                targetPlanet.GetComponentInChildren<LineRenderer>().SetColors(normalColor, normalColor);
                GameObject.Destroy(currentTrajectory.gameObject);
                currentTrajectory = null;
                targetPlanet = null;
            }
            targetPlanet = parPlanet;
            targetPlanet.GetComponentInChildren<LineRenderer>().SetColors(targetColor, targetColor);

            ComputeTrajectoryBetween(sourcePlanet, targetPlanet);
        }
    }

    private void ComputeTrajectoryBetween(PlanetLayout sourcePlanet, PlanetLayout targetPlanet)
    {
        currentTrajectory = GameObject.Instantiate<Trajectory>(trajectoryPrefab, sourcePlanet.gameObject.transform);
        currentTrajectory.SetTargetPlanet(targetPlanet);
    }
}