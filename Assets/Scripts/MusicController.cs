﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{

    [ SerializeField ] private List<AudioSource> musicList;

    private AudioSource currentMusic;

    private void Awake()
    {
        foreach( AudioSource music in musicList )
        {
            music.loop = false;
            music.playOnAwake = false;
        }

        SelectRandomMusic();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if( !currentMusic.isPlaying && musicList.Count > 0)
        {
            SelectRandomMusic();
        }
    }

    private void SelectRandomMusic()
    {
        if( musicList.Count != 0 )
        {
            int index = Mathf.FloorToInt( Random.Range( 0, musicList.Count ) );

            if( musicList.Count > 1 )
            {
                while( musicList[ index ] == currentMusic )
                {
                    index = Mathf.FloorToInt( Random.Range( 0, musicList.Count ) );
                }
            }
            currentMusic = musicList[ index ];
            currentMusic.Play();
        }
    }
}
