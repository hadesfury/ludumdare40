﻿
    public enum MessageType
    {
        GAME_START,
        GAME_OVER,
        GAME_INTRO,
        RESET
    }