﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtSwitcher : MonoBehaviour {

    [SerializeField] private LookAt lookAt;
    private Camera cam;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update ()
	{
	    cam = GameManager.gameManager.GetActiveCamera();
        lookAt.target = cam.transform;
	}
}
