﻿using System.Collections.Generic;
using UnityEngine;

public class SolarSystemLayout : MonoBehaviour
{
    [SerializeField] private CameraFocus systemCamera;
    [SerializeField] private Transform planetDock;

    private SolarSystem solarSystem;

    [SerializeField]
    private PlanetLayout planetPrefab;

    [SerializeField]
    private SunLayout sunPrefab;

    private List<PlanetLayout> planetLayouts = new List<PlanetLayout>();

    private void Awake()
    {
        solarSystem = new SolarSystem();
        solarSystem.Generate();

        SunLayout locSunLayout = GameObject.Instantiate(sunPrefab);

        foreach (Planet locPlanet in solarSystem.Planets)
        {
            PlanetLayout locNewPlanetLayout = GameObject.Instantiate(planetPrefab);
            locNewPlanetLayout.transform.SetParent(planetDock, false);
            locNewPlanetLayout.Planet = locPlanet;
            locPlanet.layout = locNewPlanetLayout;

            locNewPlanetLayout.transform.localPosition = new Vector3( locNewPlanetLayout.Planet.GetSunDistance(), 0, 0 ); // TODO
            locNewPlanetLayout.AdaptScaleFromPlanetSize();

            planetLayouts.Add(locNewPlanetLayout);
        }
    }

    public List<PlanetLayout> GetPlanetLayouts()
    {
        return planetLayouts;
    }

    public SolarSystem GetSolarSystem()
    {
        return solarSystem;
    }

    public CameraFocus GetSystemCamera()
    {
        return systemCamera;
    }

    public PlanetLayout GetRandomPlanet()
    {
        return planetLayouts[Random.Range(0, planetLayouts.Count)];
    }

    public List<PlanetLayout> GetColonizedPlanetTable()
    {
        List<PlanetLayout> locColonizedPlanetTable = new List<PlanetLayout>();

        foreach (PlanetLayout locPlanetLayout in planetLayouts)
        {
            if (locPlanetLayout.Planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED)
            {
                locColonizedPlanetTable.Add(locPlanetLayout);
            }
        }
        
        return locColonizedPlanetTable;
    }

    public PlanetLayout GetPlanetByName(string parPlanetName)
    {
        PlanetLayout locResultPlanetLayout = null;
        
        foreach (PlanetLayout locPlanetLayout in planetLayouts)
        {
            if (locPlanetLayout.Planet.GetName() == parPlanetName)
            {
                locResultPlanetLayout = locPlanetLayout;
            }
        }

        return locResultPlanetLayout;
    }
}