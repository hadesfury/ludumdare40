﻿using System;
using System.Collections.Generic;
using Data;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlanetLayout : MonoBehaviour
{
    private Planet planet;
    private static float position_to_rad = (360 * Mathf.PI / 180);

    [SerializeField] private CameraFocus planetCamera;

    [SerializeField] private ComponentCollider componentCollider;
    [SerializeField] private LineRenderer orbit;
    [SerializeField] private float scaleBase = 2;
    [SerializeField] private float maxScaleFromPlanetSize = 6;

    [SerializeField] private Transform planetMesh;
    [SerializeField] private Transform colonizedAddOn;
    
    [SerializeField] private Canvas planetWorldInfoCanvas;
    [SerializeField] private TMP_Text planetNameText;
    [SerializeField] private Transform planetInfoDock;

    [Header("Ship Prefabs")] 
    [SerializeField] private Ship colonyShip;
    [SerializeField] private Ship cargoShip;

    [Header("Planet parameter")] 
    [SerializeField] private MeshRenderer innerShereRenderer;
    [SerializeField] private MeshRenderer outerSphereRenderer;
    [SerializeField] private List<Material> planetMaterialTable;
    [SerializeField] private List<Color> planetColorTable;

    private void Awake()
    {
        Color locInnerColor = planetColorTable[Random.Range(0, planetColorTable.Count)];
        Color locOuterColor;

        do
        {
            locOuterColor = planetColorTable[Random.Range(0, planetColorTable.Count)];
        }
        while (locInnerColor == locOuterColor);
        
        innerShereRenderer.material.color = locInnerColor;
        innerShereRenderer.material.SetColor("_EmissionColor", locInnerColor);
        
        outerSphereRenderer.material = planetMaterialTable[Random.Range(0, planetMaterialTable.Count)];
        outerSphereRenderer.material.color = locOuterColor;
        outerSphereRenderer.material.SetColor("_EmissionColor", locOuterColor);
    }

    private void OnEnable()
    {
        componentCollider.onMouseDown += OnMouseDown;
    }

    private void OnDisable()
    {
        componentCollider.onMouseDown -= OnMouseDown;
    }

    private void Update()
    {
        planet.Tick(Time.deltaTime);
        
        if (GameManager.gameManager.IsCurrentState<IngameState>())
        {
            planet.IngameTick(Time.deltaTime);
        }

        Vector3 locNewPosition = new Vector3
        {
            x = planet.GetSunDistance() * Mathf.Cos( planet.GetOrbitalPosition() * position_to_rad ),
            y = 0,
            z = planet.GetSunDistance() * Mathf.Sin( planet.GetOrbitalPosition() * position_to_rad )
        };

        transform.localPosition = locNewPosition;

        colonizedAddOn.gameObject.SetActive(planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED);

        planetWorldInfoCanvas.transform.position = planetInfoDock.position;
        planetWorldInfoCanvas.transform.LookAt(GameManager.gameManager.GetActiveCamera().transform.position);
        
        planetNameText.SetText(planet.GetName());

        if( planet.GetPlanetStatus() == Planet.PlanetStatus.COLONIZED )
        {
            orbit.startColor = new Color( 0.568f, 0.976f, 0.678f );
            orbit.endColor = new Color( 0.568f, 0.976f, 0.678f );

            colonizedAddOn.gameObject.SetActive( true );
        }

        else
        {
            orbit.startColor = new Color( 1, 1, 1 );
            orbit.endColor = new Color( 1, 1, 1 );

            colonizedAddOn.gameObject.SetActive( false );
        }
        
    }

    public Planet Planet
    {
        get { return planet; }
        set
        {
            planet = value;
        }
    }

    private void OnMouseDown()
    {
        GameManager.gameManager.SelectPlanet( this );
    }

    public void Highlight(bool b)
    {
        orbit.startWidth = b ? 1 : 0.1f;
        orbit.endWidth = b ? 1 : 0.1f;
    }

    public void AdaptScaleFromPlanetSize()
    {
        int locPlanetSizeDelta = Planet.MAX_PLANETS_SIZE - Planet.MIN_PLANETS_SIZE;
        float locScale = scaleBase + ((float) planet.GetSize() - Planet.MIN_PLANETS_SIZE) * (maxScaleFromPlanetSize - scaleBase) / locPlanetSizeDelta;
        planetMesh.localScale = Vector3.one * locScale;
    }

    public void SendPopulationShipTo(PlanetLayout parDestination)
    {
        if( Planet.GetPopulation() >= Planet.COLONY_SHIP_POPULATION_COUNT)
        {
            Planet.SetPopulation( Planet.GetPopulation() - Planet.COLONY_SHIP_POPULATION_COUNT );
            
            SendShip(parDestination, Planet.COLONY_SHIP_POPULATION_COUNT, colonyShip);
        }
    }

    public void SendCargoShipTo(PlanetLayout parDestination)
    {
        if( Planet.GetCollectedResources() >= Planet.CARGO_SHIP_RESOURCE_COUNT)
        {
            Planet.SetCollectedResources( Planet.GetCollectedResources() - Planet.CARGO_SHIP_RESOURCE_COUNT );
            
            SendShip(parDestination, Planet.CARGO_SHIP_RESOURCE_COUNT, cargoShip);
        }
    }

    private void SendShip(PlanetLayout parDestination, int parShipmentQuantity, Ship parShipPrefab)
    {
        Ship locColonyShip = GameObject.Instantiate(parShipPrefab, transform.position, Quaternion.identity);
        
        locColonyShip.SetShipmentQuantity(parShipmentQuantity);
        locColonyShip.SetDestination(parDestination);
        locColonyShip.Launch();
    }

    public CameraFocus GetPlanetCamera()
    {
        return planetCamera;
    }
}
