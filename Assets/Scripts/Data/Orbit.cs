﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Orbit : MonoBehaviour
{
    [Range(3, 256)] public int numSegments = 256;

    private PlanetLayout planetLayout;
    private SolarSystemLayout solarSystemLayout;
    private LineRenderer lineRenderer;

    private void Start()
    {
        planetLayout = GetComponentInParent<PlanetLayout>();
        solarSystemLayout = planetLayout.GetComponentInParent<SolarSystemLayout>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetVertexCount(numSegments + 1);
        lineRenderer.useWorldSpace = false;
    }

    private void Update()
    {
        transform.position = solarSystemLayout.transform.position;

        float deltaTheta = (float) (2.0 * Mathf.PI) / numSegments;
        float theta = 0f;

        for (int i = 0; i < numSegments + 1; i++)
        {
            float x = planetLayout.Planet.GetSunDistance() * Mathf.Cos( theta );
            float z = planetLayout.Planet.GetSunDistance() * Mathf.Sin( theta );
            Vector3 pos = new Vector3(x, 0, z);
            lineRenderer.SetPosition(i, pos);
            theta += deltaTheta;
        }
    }
}