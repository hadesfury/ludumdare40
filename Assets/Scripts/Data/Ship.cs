﻿ using TMPro;
 using UnityEngine;
 using UnityEngine.UI;

namespace Data
{
    public class Ship : MonoBehaviour
    {
        public enum ShipContent
        {
            POPULATION,
            RESOURCE
        }

        [SerializeField] private float speed = 100.0f;
        [SerializeField] private const float acceleration = 1.0f;
        [SerializeField] private ShipContent shipContent;
        [SerializeField] private CameraFocus cameraFocus;
        [SerializeField] private Canvas shipWorldInfoCanvas;
        [SerializeField] private TMP_Text shipNameText;
        [SerializeField] private Button shipSelectionButton;

        private PlanetLayout destination;
        private int shipmentQuantity;
        private bool isLaunched = false;
        private bool isCameraFocused = false;

        private void Awake()
        {
            shipSelectionButton.onClick.AddListener( OnShipSelection );
        }


        private void FixedUpdate()
        {
            if (isLaunched)
            {
                Vector3 locDirection = destination.transform.position - transform.position;

                if (locDirection.sqrMagnitude < 1)
                {
                    if (shipContent == ShipContent.POPULATION)
                    {
                        string locNotificationText = string.Format("SHIP HAS ARRIVED - Planet {0} received {1} workers.", destination.Planet.GetName(), shipmentQuantity);
                        
                        GameManager.gameManager.PostNotification(locNotificationText);
                        destination.Planet.ReceivePopulationShipment(shipmentQuantity);
                    }
                    else if (shipContent == ShipContent.RESOURCE)
                    {
                        string locNotificationText = string.Format("SHIP HAS ARRIVED - Planet {0} received {1} resources.", destination.Planet.GetName(), shipmentQuantity);
                        
                        GameManager.gameManager.PostNotification(locNotificationText);
                        destination.Planet.ReceiveResourceShipment(shipmentQuantity);
                    }

                    if( isCameraFocused )
                    {
                        GameManager.gameManager.SelectPlanet( destination );
                    }

                    GameObject.Destroy(gameObject);
                }
                else
                {
                    transform.LookAt(destination.transform);
                    transform.Translate(locDirection.normalized * Time.deltaTime * speed, Space.World);
                    
                    shipWorldInfoCanvas.transform.LookAt(GameManager.gameManager.GetActiveCamera().transform.position);

                    speed += acceleration * Time.fixedDeltaTime;
                }
            }
        }



        public void SetDestination(PlanetLayout parPlanetLayout)
        {
            destination = parPlanetLayout;
        }

        public void SetShipmentQuantity(int parShipmentQuantity)
        {
            shipmentQuantity = parShipmentQuantity;
        }

        public void Launch()
        {
            isLaunched = true;

            if (shipContent == ShipContent.POPULATION)
            {
                shipNameText.SetText(string.Format("Colony Ship (Destination {0})", destination.Planet.GetName()));
            }
            else
            {
                shipNameText.SetText(string.Format("Cargo Ship (Destination {0})", destination.Planet.GetName()));
            }
        }

        private void OnShipSelection()
        {
            isCameraFocused = true;
            shipNameText.fontSize = 4.0f;
            GameManager.gameManager.SelectShip(this);
        }


        public CameraFocus GetCamera()
        {
            return cameraFocus;
        }
    }
}