﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour {

    [Range(3, 256)]
    public int numSegments = 256;

    private PlanetLayout sourcePlanet;
    private PlanetLayout targetPlanet;
    private SolarSystemLayout solarSystemLayout;
    private LineRenderer lineRenderer;
    private int distance;

    public void SetTargetPlanet(PlanetLayout targetPlanet)
    {
        this.targetPlanet = targetPlanet;
    }

    private void Start()
    {
        sourcePlanet = GetComponentInParent<PlanetLayout>();
        distance = Math.Abs( sourcePlanet.Planet.GetSunDistance() - targetPlanet.Planet.GetSunDistance() );

        solarSystemLayout = sourcePlanet.GetComponentInParent<SolarSystemLayout>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetVertexCount(numSegments + 1);
        lineRenderer.useWorldSpace = false;
    }

    private void Update()
    {
        float way = sourcePlanet.Planet.GetSunDistance() <= targetPlanet.Planet.GetSunDistance() ? -1 : 1;
        Vector3 heading = (sourcePlanet.transform.position - solarSystemLayout.transform.position) / 2 ;
        Vector3 direction = heading / heading.magnitude * way;


        transform.position = Vector3.MoveTowards(sourcePlanet.transform.position, solarSystemLayout.transform.position, distance * way / 2);
        transform.localRotation = Quaternion.Euler(0, 180-(360 * sourcePlanet.Planet.GetOrbitalPosition()), 0);

        float deltaTheta = (float)(2.0 * Mathf.PI) / numSegments;
        float theta = 0f;

        for (int i = 0; i < numSegments / 2 + 1; i++)
        {
            float x = distance / 2 * Mathf.Cos(theta);
            float y = distance / 2 * Mathf.Sin(theta);
            Vector3 pos = new Vector3(x, y, 0);
            lineRenderer.SetPosition(i, pos);
            theta += deltaTheta;
        }
    }
}
