﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SolarSystem
{
    public event Action onPlanetsGenerated;

    private readonly  Sun sun = new Sun();

    private readonly List<Planet> planets = new List<Planet>();

    private static readonly int MIN_PLANETS_COUNT = 8;
    private static readonly int MAX_PLANETS_COUNT = 8;

    private static readonly float MIN_PLANET_ORBITAL_PROGRESSION = 0.01f;
    private static readonly float MAX_PLANET_ORBITAL_PROGRESSION = 0.06f;

    private static readonly int MIN_SUN_DISTANCE = 20;
    private static readonly int MAX_SUN_DISTANCE = 150;
    private static readonly int MIN_DISTANCE_BETWEEN_PLANETS = 6;

    public void Generate()
    {
        planets.Clear();

        int locPlanetsCount = Random.Range(MIN_PLANETS_COUNT, MAX_PLANETS_COUNT);
        int locMinOrbitCircumference = MathUtilities.GetCircumference(MIN_SUN_DISTANCE);

        for (int locPlanetIndex = 0; locPlanetIndex < locPlanetsCount; locPlanetIndex++)
        {
            int locSunDistance;
            bool locHasExistingPlanetNearby;
            int locTryCount = 0;

            do
            {
                locSunDistance =  Random.Range(MIN_SUN_DISTANCE, MAX_SUN_DISTANCE);// MathUtilities.GetRandomGaussian(MIN_SUN_DISTANCE, MAX_SUN_DISTANCE);

                locHasExistingPlanetNearby = false;

                foreach (Planet locPlanet in planets)
                {
                    if( Math.Abs( locPlanet.GetSunDistance() - locSunDistance ) < MIN_DISTANCE_BETWEEN_PLANETS )
                    {
                        locHasExistingPlanetNearby = true;
                    }
                }

                ++locTryCount;
            } while (locHasExistingPlanetNearby && (locTryCount < 10));

            if (!locHasExistingPlanetNearby)
            {
                float locOrbitProgressionDistanceFactor = ((float) MathUtilities.GetCircumference(locSunDistance) / locMinOrbitCircumference);
                float locOrbitalProgression = MathUtilities.GetRandomGaussian(MIN_PLANET_ORBITAL_PROGRESSION, MAX_PLANET_ORBITAL_PROGRESSION) / locOrbitProgressionDistanceFactor;
                string locPlanetName = GameManager.gameManager.GetUniquePlanetName();
                Planet locNewPlanet = new Planet(locPlanetName, locSunDistance, locOrbitalProgression);
                
                planets.Add(locNewPlanet);
            }
            else
            {
                Debug.LogError("Some planets have not been generated : too many iterations");
            }
        }

        planets.Sort();
    }

    public List<Planet> Planets
    {
        get { return planets; }
    }
}
