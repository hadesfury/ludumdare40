﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class Planet : IComparable<Planet>
{
    public enum PlanetStatus
    {
        NOT_COLONIZED,
        COLONIZED
    }

    public enum AvailabeResourcesStatus
    {
        ALRIGHT = 0,
        WARNING = 1,
        DANGER = 2,
        EMPTY = 3
    }

    public event Action<PlanetStatus, PlanetStatus, Planet> onPlanetStatusChanged; //Sends event including old planet status, new planet status and ID of the planet
    public event Action<Planet> onPlanetColonized;

    public static readonly int COLONY_SHIP_POPULATION_COUNT = 50;
    public static readonly int CARGO_SHIP_RESOURCE_COUNT = 300;
    public static readonly float POPULATION_RESOURCES_CONSUMPTION_RATE = 0.05f;
    public static readonly int MINE_PRODUCTION_RATE = 10;
    public static readonly int MINE_COST = 250;
    public static readonly int MINE_WORKER_SLOT = 50;
    public static readonly int GROWTH_FACTOR_MIN = 3;
    public static readonly int GROWTH_FACTOR_MAX = 5;
    public static readonly int STARVING_GROWTH_FACTOR = -1;

    public static readonly int MIN_PLANETS_AVAILABLE_RESOURCES = 2000;
    public static readonly int MAX_PLANETS_AVAILABLE_RESOURCES = 10000;
    public static readonly int PLANET_AVAILABLE_RESOURCES_WARNING = 3000;
    public static readonly int PLANET_AVAILABLE_RESOURCES_DANGER = 1500;

    public static readonly int MIN_PLANETS_SIZE = 2;
    public static readonly int MAX_PLANETS_SIZE = 6;

    public static readonly float WORKER_PRODUCTION_RATE = MINE_PRODUCTION_RATE / (float) MINE_WORKER_SLOT;
    
    private static readonly IncrementalId incrementalId = new IncrementalId();

    private readonly long id;
    private readonly int sunDistance;
    private float orbitalProgression;
    private float orbitalPosition;

    private string name = "Planet";
    private PlanetStatus planetStatus = PlanetStatus.NOT_COLONIZED;
    private int population = 0;
    private int populationGrowthFactor = 0;

    private int richness;
    private int size;
    private int availableResources;
    private int collectedResources;
    private int spentResources;
    
    private readonly List<Mine> mineTable = new List<Mine>();
    private float currentMinedResources;
    private float currentConsumedResources;
    private float currentPopulationGrowth;
    private int currentPopulationGrowthFactor;

    private AvailabeResourcesStatus availabeResourcesStatus = AvailabeResourcesStatus.ALRIGHT;

    public PlanetLayout layout;

    public Planet(string parName, int parSunDistance, float parOrbitalProgression)
    {
        id = incrementalId.GetNextId();

        sunDistance = parSunDistance;
        orbitalProgression = parOrbitalProgression;

        orbitalPosition = Random.Range(0.0f, 1.0f);
        name = parName;

        availableResources = Mathf.RoundToInt( Random.Range( MIN_PLANETS_AVAILABLE_RESOURCES, MAX_PLANETS_AVAILABLE_RESOURCES ) );
        size = Mathf.RoundToInt(Random.Range(MIN_PLANETS_SIZE, MAX_PLANETS_SIZE));

        populationGrowthFactor = Mathf.RoundToInt(Random.Range( GROWTH_FACTOR_MIN, GROWTH_FACTOR_MAX ));
        currentPopulationGrowthFactor = populationGrowthFactor;
    }

    public void Tick(float parDeltaTime)
    {
        orbitalPosition += orbitalProgression * parDeltaTime;

        if (orbitalPosition >= 1.0f)
        {
            orbitalPosition = 0.0f; // start a new orbit (happy new year!)
        }
    }

    public void IngameTick(float parDeltaTime)
    {
        if (population > 0)
        {
            float locMineProductionRate = GetYield();
            
            currentMinedResources +=  locMineProductionRate * parDeltaTime;
            currentConsumedResources += population * POPULATION_RESOURCES_CONSUMPTION_RATE * parDeltaTime;

            if (currentMinedResources >= 1.0f && availableResources != 0)
            {
                int locMinedResourceCount = Mathf.FloorToInt(currentMinedResources);
                
                currentMinedResources -= locMinedResourceCount;
                collectedResources += locMinedResourceCount;
                availableResources -= locMinedResourceCount;

                if( availableResources < 0 )
                {
                    availableResources = 0;
                }
            }

            int locConsumedResources = Mathf.FloorToInt( currentConsumedResources );
            currentConsumedResources -= locConsumedResources;
            collectedResources -= locConsumedResources;

            if( collectedResources < 0 )
            {
                collectedResources = 0;
            }



            if( collectedResources == 0 )
            {
                currentPopulationGrowthFactor = STARVING_GROWTH_FACTOR;
            }

            else
            {
                currentPopulationGrowthFactor = populationGrowthFactor;
            }
            
            currentPopulationGrowth += currentPopulationGrowthFactor * parDeltaTime;

            if (population > 1 || currentPopulationGrowth < 0)
            {
                if( currentPopulationGrowth >= 1.0f || currentPopulationGrowth <= -1.0f )
                {
                    int locPopulationGrowth = Mathf.FloorToInt(currentPopulationGrowth);
    
                    currentPopulationGrowth -= locPopulationGrowth;
                    population += locPopulationGrowth;
                }
            }

            if( population < 0 )
            {
                population = 0;
            }

            if( population == 0 )
            {
                SetPlanetStatus( PlanetStatus.NOT_COLONIZED );
            }



            if( availableResources < PLANET_AVAILABLE_RESOURCES_WARNING && GetPlanetAvailabeResourcesStatus() < AvailabeResourcesStatus.WARNING)
            {
                GameManager.gameManager.PostNotification( string.Format( "<color=yellow>WARNING - {0} resources getting low !", GetName() ) );
                availabeResourcesStatus = AvailabeResourcesStatus.WARNING;
            }

            if( availableResources < PLANET_AVAILABLE_RESOURCES_DANGER && GetPlanetAvailabeResourcesStatus() < AvailabeResourcesStatus.DANGER )
            {
                GameManager.gameManager.PostNotification( string.Format( "<color=red>DANGER - {0} resources getting extremely low !", GetName() ) );
                availabeResourcesStatus = AvailabeResourcesStatus.DANGER;
            }

            if( availableResources == 0 && GetPlanetAvailabeResourcesStatus() < AvailabeResourcesStatus.EMPTY )
            {
                GameManager.gameManager.PostNotification( string.Format( "<color=red>No more resources available on {0} !", GetName() ) );
                availabeResourcesStatus = AvailabeResourcesStatus.EMPTY;
            }
        }
    }

    public Mine BuildMine()
    {
        Mine locNewMine = null;

        if (collectedResources >= MINE_COST)
        {
            locNewMine = new Mine();

            collectedResources -= MINE_COST;
            spentResources += MINE_COST;
            
            mineTable.Add(locNewMine);
        }
        
        return locNewMine;
    }

    public void ReceiveResourceShipment( int parAmount )
    {
        Debug.Log( "Planet " + name + " received " + parAmount + " resources." );
        collectedResources += parAmount;
    }

    public void ReceivePopulationShipment( int parAmount )
    {
        Debug.Log( "Planet " + name + " received " + parAmount + " citizens." );
        population += parAmount;
        SetPlanetStatus( PlanetStatus.COLONIZED );
    }

    private void SetPlanetStatus( PlanetStatus parNewStatus )
    {
        if (planetStatus != parNewStatus)
        {
            PlanetStatus locPreviousStatus = planetStatus;
            
            planetStatus = parNewStatus;
            
            if (parNewStatus == PlanetStatus.COLONIZED)
            {
                if (onPlanetColonized != null)
                {
                    onPlanetColonized(this);
                }
            }
            
            if( onPlanetStatusChanged != null )
            {
                onPlanetStatusChanged( locPreviousStatus, parNewStatus, this );
            }
        }
    }

    public long GetId()
    {
        return id;
    }

    public int GetSunDistance()
    {
        return sunDistance;
    }


    public void SetOrbitalProgression( float value )
    {
        orbitalProgression = value;
    }

    public float GetOrbitalProgression()
    {
        return orbitalProgression;
    }


    public void SetOrbitalPosition( float value )
    {
        orbitalPosition = value;
    }

    public float GetOrbitalPosition()
    {
        return orbitalPosition;
    }

    public void SetRichness( int value )
    {
        richness = value;
    }

    public int GetRichness()
    {
        return richness;
    }

    public void SetSize( int value )
    {
        size = value;
    }

    public int GetSize()
    {
        return size;
    }

    public void SetAvailableResources( int value )
    {
        availableResources = value;
    }

    public int GetAvailableResources()
    {
        return availableResources;
    }

    public void SetCollectedResources( int value )
    {
        collectedResources = value;
    }

    public int GetCollectedResources()
    {
        return collectedResources;
    }

    public float GetResourcesRatio()
    {
        return GetYield() - ( population * POPULATION_RESOURCES_CONSUMPTION_RATE );
    }

    public int GetMineCount()
    {
        return mineTable.Count;
    }

    public int GetWorkingMineCount()
    {
        int locPotentialWorkingMines = ( int ) Mathf.Ceil( ( float ) GetPopulation() / ( float ) MINE_WORKER_SLOT );

        if( GetMineCount() < locPotentialWorkingMines )
        {
            return GetMineCount();
        }

        else
        {
            return locPotentialWorkingMines;
        }
    }

    public float GetYield()
    {
        return WORKER_PRODUCTION_RATE * Mathf.Min( population, MINE_WORKER_SLOT * mineTable.Count );
    }

    public void SetPopulation( int value )
    {
        population = value;
    }

    public int GetPopulation()
    {
        return population;
    }

    public void SetPopulationGrowthFactor( int value )
    {
        currentPopulationGrowthFactor = value;
    }

    public int GetPopulationGrowthFactor()
    {
        return currentPopulationGrowthFactor;
    }

    public bool GetIsStarving()
    {
        if( currentPopulationGrowthFactor < 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetName( string value )
    {
        name = value;
    }

    public string GetName()
    {
        return name;
    }

    public int GetUnemployedCitizens()
    {
        int locUnemployed = population - ( GetMineCount() * MINE_WORKER_SLOT );

        if( locUnemployed < 0 )
        {
            locUnemployed = 0;
        }

        return locUnemployed;
    }

    public int GetWorkingCitizens()
    {
        return GetPopulation() - GetUnemployedCitizens();
    }

    public int GetTotalMineSlots()
    {
        return GetMineCount() * MINE_WORKER_SLOT;
    }

    public AvailabeResourcesStatus GetPlanetAvailabeResourcesStatus()
    {
        return availabeResourcesStatus;
    }

    public PlanetStatus GetPlanetStatus()
    {
        return planetStatus;
    }

    public int CompareTo(Planet other)
    {
        return this.GetSunDistance() < other.GetSunDistance() ? -1 : 1;
    }
}
